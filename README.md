# Some References

- terraform-provider-rke https://github.com/rancher/terraform-provider-rke/tree/master/examples/aws_ec2
- https://medium.com/@brotandgames/deploy-a-kubernetes-cluster-using-terraform-and-rke-provider-68112463e49d

# Prerequisites
- terraform v0.12
- terraform-provider-rke latest
- Valid AWS access_key and secret_key
- kubectl command
- helm command

# Steps

## Setup RKE
download terraform-provider-rke plugin, and put in the dir
- `terraform init`
- `terraform plan`
- `terraform apply`
sometimes got error `/terraform/blabla.sh`, it may be due to failed ssh connection, just retry `terraform apply`

## Deploy PMA
- `helm repo add bitnami https://charts.bitnami.com/bitnami`
- `helm install my-release bitnami/phpmyadmin`
- `kubectl port-forward --namespace default svc/my-release-phpmyadmin 8080:80`
- `helm upgrade my-release bitnami/phpmyadmin --set db.host=mydb`

## Deploy Devpanel Wordpress
https://github.com/vothanhbinhlt/devpanel-wordpress-auto

## Setup Rancher Server

# How-To

## Setup RKE Cluster
```
export AWS_ACCESS_KEY_ID=XXXXXX
export AWS_SECRET_ACCESS_KEY=YYYYYYY
```

Install terraform-provider-rke plugin
```
OS="linux"
PLUGIN_RELEASE="terraform-provider-rke_$OS-amd64"
PLUGIN_URL="https://github.com/rancher/terraform-provider-rke/releases/download/1.0.0-rc5/$PLUGIN_RELEASE"
PLUGIN_PATH=".terraform/plugins/$OS_amd64"
wget --quiet $PLUGIN_URL -P $PLUGIN_PATH \
  && mv $PLUGIN_PATH/$PLUGIN_RELEASE $PLUGIN_PATH/terraform-provider-rke \
  && chmod +x $PLUGIN_PATH/terraform-provider-rke
```

terraform init
terraform plan
terraform apply