output "private_key" {
  value = tls_private_key.node-key.private_key_pem
}

output "ssh_username" {
  value = "ubuntu"
}

output "master_addresses" {
  value = aws_instance.rke-master[*].public_dns
}

output "master_internal_ips" {
  value = aws_instance.rke-master[*].private_ip
}

output "addresses" {
  value = aws_instance.rke-node[*].public_dns
}

output "internal_ips" {
  value = aws_instance.rke-node[*].private_ip
}